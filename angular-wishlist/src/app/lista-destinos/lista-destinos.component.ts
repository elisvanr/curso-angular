import { Component, OnInit } from '@angular/core';
import {DestinoViaje} from './../models/destino-viaje.model';

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css']
})
export class ListaDestinosComponent implements OnInit {
  Destinos: DestinoViaje[];
  constructor() { 
    this.Destinos=[];

  }

  ngOnInit(): void {

    
  }
  guardar(nombre:string,url:string):boolean{
    this.Destinos.push(new DestinoViaje(nombre,url));
    
    return false;
  }

}
